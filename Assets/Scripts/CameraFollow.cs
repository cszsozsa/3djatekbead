﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;

    public Vector3 offset;
    public float smoothTime = .5f;

    private Vector3 velocity;

    void LateUpdate()
    {
        Move();
    }

    void Move()
    {
        Vector3 centerPoint =target.position;

        Vector3 newPosition = centerPoint + offset;

        transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref velocity, smoothTime);
    }

}
